---
name: investigation_encoded_2
points: 500
category: forensics
author: ewn10
---

Another binary that's encoding the flag into an output file. This challenge is
almost identical to Investigation Encoded 1, except it has a larger character
set and does some funky math with each character before encoding it.

## Reversing the encoder

Looking at the `encode` function, we see it's very similar to the one from
InvEnc1, except `matrix` is now `indexTable` and structured slightly
differently. We can simply transcribe the decompiled output from Ghidra for most
of this process. And just like with InvEnc1, we have a `getValue` function that
Ghidra has a real hard time figuring out:

```c
ulong getValue(int _val)

{
  byte bVar1;
  int iVar2;

  iVar2 = _val;
  if (_val < 0) {
    iVar2 = _val + 7;
  }
  bVar1 = (byte)(_val >> 0x37);
  return (ulong)((int)(uint)secret[(long)(iVar2 >> 3)] >>
                 (7 - (((char)_val + (bVar1 >> 5) & 7) - (bVar1 >> 5)) & 0x1f) & 1);
}
```

However, by playing around with the values of each of these chunks of math we
can figure out that this is actually getting the `_val`th bit of `secret`, which
simplifies our encoder implementation a bit:

```python
def getValue(val):
    return (secret[val>>3] >> (7 - val & 7)) & 1
```

We can also check this value against transcribing the original Ghidra decompiler
output, which shows that, at least for the values of `val` that are used in the
course of the encoder running, our simplified version is accurate.

We also need the values of the constant arrays `indexTable` and `secret`, which
I got into Python by selecting "copy as byte string (no spaces)" in Ghidra and
then using `binascii.unhexlify` to get into an actual array (and also unpacking
ints for `indexTable`, which I named `matrix` since I was reusing my code from
the previous challenge):

```python
import struct
from binascii import unhexlify

secret = unhexlify('8baa2eeee8bbae8ebbae3aee8eeea8eeaee3aae3aebb8baeb8eaae2eba2eae8aeea3aba3bbbb8bbbb8aeee2aee2e2ab8aa8eaa3baa3bba8ea8eba3a8aa28bbb8ae2ae2ee3ab800')
_matrix = unhexlify('000000000400000012000000280000003c0000005200000064000000780000008e0000009e000000b4000000c8000000da000000ea000000fc0000000e0100001e01000034010000480100005a0100006a01000072010000800100008c0100009a010000aa010000bc010000c8010000d6010000e0010000ea010000f0010000000200000a02000016020000220200003002000034020000')

matrix = []
for i in range(0, len(_matrix), 4):
    matrix += struct.unpack('I', _matrix[i:i+4])
```

Now we have a full encoder implementation, which we can test by comparing
outputs against the binary:

```python
#!/usr/bin/env python3
# encode.py
import struct
from binascii import unhexlify

secret = unhexlify('8baa2eeee8bbae8ebbae3aee8eeea8eeaee3aae3aebb8baeb8eaae2eba2eae8aeea3aba3bbbb8bbbb8aeee2aee2e2ab8aa8eaa3baa3bba8ea8eba3a8aa28bbb8ae2ae2ee3ab800')
_matrix = unhexlify('000000000400000012000000280000003c0000005200000064000000780000008e0000009e000000b4000000c8000000da000000ea000000fc0000000e0100001e01000034010000480100005a0100006a01000072010000800100008c0100009a010000aa010000bc010000c8010000d6010000e0010000ea010000f0010000000200000a02000016020000220200003002000034020000')

matrix = []
for i in range(0, len(_matrix), 4):
    matrix += struct.unpack('I', _matrix[i:i+4])

def getValue(val):
    return (secret[val>>3] >> (7 - val & 7)) & 1

def sbox(ch):
    v1 = (ch + 0x12) % 0x24
    ch = v1 >> 0x1f & 0xff
    ch = (v1 ^ ch) - ch
    return ch

def encode(flag):
    output = b''

    flag_index = 0
    remain = 7
    bufChar = 0

    def save(b):
        nonlocal bufChar
        nonlocal remain
        nonlocal output
        bufChar |= b
        if remain == 0:
            remain = 7
            output += bytes([bufChar & 0xff])
            bufChar = 0
        else:
            bufChar *= 2
            remain -= 1

    while True:
        if len(flag) <= flag_index:
            while (remain != 7):
                save(0)
            return output
        ch = ord(flag[flag_index:flag_index + 1].lower())
        if ch == 0x20:
            ch = 0x85
        elif ch >= ord('0') and ch <= ord('9'):
            ch += 0x4b
        ch -= 0x61
        if ch < 0 or ch > 36:
            raise ValueError('Flag contains invalid characters')
        if ch != 0x24: # not space
            ch = sbox(ch)
        v1 = matrix[ch]
        v2 = matrix[ch + 1]
        while (v1 < v2):
            val = getValue(v1)
            save(val & 0xff)
            v1 += 1
        flag_index += 1

if __name__ == '__main__':
    import sys
    with open(sys.argv[1], 'r') as fi, open(sys.argv[2], 'wb') as fo:
        fo.write(encode(fi.read()))
```

## Decoding

Like the previous challenge, this binary converts each character in to a
(variable length) sequence of bits, which get packed into each character in the
output file MSB-first (so the MSB of the first byte is the first byte written).
We can convert the raw incoming characters to an array of bits like this:

```python
bits = list(itertools.chain(
    *[[(ch >> i) & 1 for i in range(7,-1,-1)] for ch in encoded]
))
```

(the `itertools.chain` is to flatten the resulting array)

We can save all of the bitstrings in a prefix tree (much like how Huffman
coding works), and thankfully all the possible bitstrings are prefix-free (none
of the bitstrings are prefixes of others). We can generate these bitstrings by
simply encoding each possible character into a bitstring---accomplished with
slight modifications of the original encoding code.

Thus, this is our final decoder (`bsToCp` is the prefix tree, implemented
through nested dictionaries):

```python
import itertools
import struct
from binascii import unhexlify

secret = unhexlify('8baa2eeee8bbae8ebbae3aee8eeea8eeaee3aae3aebb8baeb8eaae2eba2eae8aeea3aba3bbbb8bbbb8aeee2aee2e2ab8aa8eaa3baa3bba8ea8eba3a8aa28bbb8ae2ae2ee3ab800')
_matrix = unhexlify('000000000400000012000000280000003c0000005200000064000000780000008e0000009e000000b4000000c8000000da000000ea000000fc0000000e0100001e01000034010000480100005a0100006a01000072010000800100008c0100009a010000aa010000bc010000c8010000d6010000e0010000ea010000f0010000000200000a02000016020000220200003002000034020000')

matrix = []
for i in range(0, len(_matrix), 4):
    matrix += struct.unpack('I', _matrix[i:i+4])

def getValue(val):
    return (secret[val>>3] >> (7 - val & 7)) & 1

def sbox(cp):
    v1 = (cp + 0x12) % 0x24
    cp = v1 >> 0x1f & 0xff
    cp = (v1 ^ cp) - cp
    return cp

# cp is 0-36 (`a-z0-9 `)
def genBitstring(cp):
    bitstring = []
    if cp != 0x24:
        cp = sbox(cp)
    v1 = matrix[cp]
    v2 = matrix[cp + 1]
    while (v1 < v2):
        bitstring.append(getValue(v1))
        v1 += 1
    return bitstring

cpToBs = [genBitstring(x) for x in range(37)]
bsToCp = {}
for cp, bs in enumerate(cpToBs):
    curr = bsToCp
    prev = ()
    for b in bs:
        try:
            curr[b]
        except KeyError:
            curr[b] = {}
        prev = (curr, b)
        curr = curr[b]
    prev[0][prev[1]] = cp

def decode(encoded):
    decoded = ''
    bits = list(itertools.chain(
        *[[(ch >> i) & 1 for i in range(7,-1,-1)] for ch in encoded]
    ))
    currentState = bsToCp
    for bit in bits:
        nextState = currentState[bit]
        if isinstance(nextState, dict):
            currentState = nextState
        else:
            if nextState == 36:
                decoded += ' '
            else:
                if nextState < 26:
                    decoded += chr(nextState + 0x61)
                else:
                    decoded += chr(nextState + 0x61 - 0x4b)
            currentState = bsToCp
    return decoded

if __name__ == '__main__':
    import sys
    with open(sys.argv[1], 'rb') as fi, open(sys.argv[2], 'w') as fo:
        fo.write(decode(fi.read()))
```
