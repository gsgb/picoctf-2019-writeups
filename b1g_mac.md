---
name: B1g_Mac
points: 500
category: forensics
author: The20thDuck
---

We are given a zip file which contains a windows executable file and a folder of images. We open
the file in Ghidra, and see that the program encodes the flag in the timestamps of the images. IMPORTANT NOTE: The zip should be unzipped with WinRAR, to preserve the original timestamps.

As a first course of action, let's look for strings. Knowing that the flag has been encoded, it is interesting to find the word "decode":

![](images/b1g_mac_strings.png)

We look at references to the string, which leads us to the function _decode at address 0x00401afe. 

![](images/b1g_mac_decode.png)

The function _decode first calls _listdir(1, folderName) which stores the flag in _buff, then calls printf to print out _buff. Sweet! All we need to do is patch the binary to call _decode and we're done! We look back to the main function to see when folderName is properly set:

![](images/b1g_mac_main.png)

folderName is set with the instruction at address 0x00401bcd so let's change the next instruction, 0x00401bd2, to jump to _decode.

![](images/b1g_mac_radare.png)

We run the patched binary and it prints out the flag!

```
picoCTF{M4cTim35!}
```